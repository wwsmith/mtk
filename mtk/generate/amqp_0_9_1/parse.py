#!/usr/bin/env python

###############################################################################
#   Copyright 2012 Warren Smith                                               #
#                                                                             #
#   Licensed under the Apache License, Version 2.0 (the "License");           #
#   you may not use this file except in compliance with the License.          #
#   You may obtain a copy of the License at                                   #
#                                                                             #
#       http://www.apache.org/licenses/LICENSE-2.0                            #
#                                                                             #
#   Unless required by applicable law or agreed to in writing, software       #
#   distributed under the License is distributed on an "AS IS" BASIS,         #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  #
#   See the License for the specific language governing permissions and       #
#   limitations under the License.                                            #
###############################################################################

import re
import sys
from xml.dom.minidom import parse

#######################################################################################################################

def getChildElements(node, name):
    elements = []
    for node in node.childNodes:
        try:
            if node.tagName == name:
                elements.append(node)
        except AttributeError:
            pass
    return elements

def getDoc(element):
    doc_elements = getChildElements(element,"doc")
    doc_list = []
    for doc_element in doc_elements:
        doc_str = doc_element.firstChild.data
        #doc_str = re.sub(r"\s+"," ",doc_str)
        doc_list.extend(doc_str.split("\n"))
    return doc_list

def getDocStr(element):
    #doc_elements = element.getElementsByTagName("doc")
    doc_elements = getChildElements(element,"doc")
    if len(doc_elements) == 0:
        return None
    doc_str = ""
    for doc_element in doc_elements:
        doc_str += doc_element.firstChild.data
    #return re.sub(r"\s+"," ",doc_str)
    return doc_str

def getRules(element):
    rules = []
    rule_elements = getChildElements(element,"rule")
    for element in rule_elements:
        rules.append(Rule(element))
    return rules

def getChassis(element):
    chassis = []
    chassis_elements = getChildElements(element,"chassis")
    for element in chassis_elements:
        chassis.append(Chassis(element))
    return chassis

#######################################################################################################################

class Constant:
    def __init__(self, element=None):
        self.name = None
        self.value = None
        self.cls = None
        self.doc = None

        if element == None:
            return

        self.name = element.getAttribute("name")
        value = element.getAttribute("value")
        try:
            self.value = int(value)
        except NumberFormatException:
            try:
                self.value = float(value)
            except NumberFormatException:
                self.value = value
        if element.hasAttribute("class"):
            self.cls = element.getAttribute("class")
        self.doc = getDoc(element)

    def pythonName(self):
        return re.sub(r"-","_",self.name).upper()
    python_name = property(pythonName)

    def __str__(self,indent=""):
        cstr = indent+"Constant "+self.name+"\n"
        if self.value:
            cstr += indent+"  value: "+str(self.value)+"\n"
        if self.cls:
            cstr += indent+"  class: "+self.cls+"\n"
        if self.doc:
            cstr += indent+"  doc: "+self.doc+"\n"
        return cstr

#######################################################################################################################

class Domain:
    def __init__(self, element=None):
        self.name = None
        self.typ = None
        self.label = None
        self.doc = None
        self.asserts = []
        self.rules = []

        if element == None:
            return

        self.name = element.getAttribute("name")
        self.typ = element.getAttribute("type")
        self.label = element.getAttribute("label")
        self.doc = getDoc(element)
        assert_elements = element.getElementsByTagName("assert")
        for element in assert_elements:
            self.asserts.append(Assert(element))
        self.rules = getRules(element)

    def __str__(self, indent=""):
        dstr = indent+"Domain "+self.name+"\n"
        dstr += indent+"  type: "+str(self.typ)+"\n"
        dstr += indent+"  label: "+self.label+"\n"
        if self.doc:
            dstr += indent+"  doc: "+self.doc+"\n"
        for asrt in self.asserts:
            dstr += asrt.__str__(indent+"  ")
        for rule in self.rules:
            dstr += rule.__str__(indent+"  ")
        return dstr

class Assert:
    def __init__(self, element=None):
        self.check = None
        self.value = None

        if element == None:
            return

        self.check = element.getAttribute("check")
        self.value = element.getAttribute("value")

    def __str__(self, indent=""):
        return indent+"Assert check "+self.check+" against "+self.value

class Rule:
    def __init__(self, element=None):
        self.name = None
        self.doc = None

        if element == None:
            return

        self.name = element.getAttribute("name")
        self.doc = getDoc(element)

    def __str__(self, indent=""):
        cstr = indent+"Rule "+self.name+": "
        if self.doc:
            cstr += indent+self.doc+"\n"
        return cstr

#######################################################################################################################

class Chassis:
    def __init__(self, element=None):
        self.name = None
        self.implement = None

        self.MUST = "MUST"
        self.SHOULD = "SHOULD"
        self.MAY = "MAY"

        if element == None:
            return

        self.name = element.getAttribute("name")
        if element.hasAttribute("implement"):
            self.implement = element.getAttribute("implement")

    def __str__(self, indent=""):
        return indent+"Chassis "+self.name+" ("+str(self.implement)+")"

class Field:
    def __init__(self, element, domains):
        self.name = None
        self.type = None
        self.domain = None
        self.label = None
        self.doc = None
        self.rules = []

        if element == None:
            return

        self.name = element.getAttribute("name")
        self.type = element.getAttribute("type")
        self.domain = element.getAttribute("domain")
        self.label = element.getAttribute("label")
        self.doc = getDoc(element)
        self.rules = getRules(element)

        if self.type:
            self.base_type = domains[self.type]
        elif self.domain:
            self.base_type = domains[self.domain]
        else:
            self.base_type = "unknown"

    def pythonName(self):
        if self.name == "global":
            return "_global"        # hack for this one python reserved word
        else:
            return re.sub(r"-","_",self.name)
    python_name = property(pythonName)

    def getType(self):
        return self.base_type


    def __str__(self, indent=""):
        fstr = indent+"Field "+self.name+"\n"
        fstr += indent+"  domain: "+self.domain+"\n"
        fstr += indent+"  label: "+self.label+"\n"
        if self.doc:
            fstr += indent+"  doc: "+self.doc
        for rule in self.rules:
            fstr += rule.__str__(indent+"  ")
        return fstr

class Method:
    def __init__(self, element, domains):
        self.name = None
        self.synchronous = False
        self.index = None
        self.label = None
        self.doc = None
        self.rules = []
        self.chassis = []
        self.responses = []
        self.response_methods = []
        self.fields = []

        if element == None:
            return

        self.name = element.getAttribute("name")
        if element.hasAttribute("synchronous"):
            self.synchronous = True
        self.index = int(element.getAttribute("index"))
        self.label = element.getAttribute("label")
        self.doc = getDoc(element)
        self.rules = getRules(element)
        self.chassis = getChassis(element)
        response_elements = element.getElementsByTagName("response")
        for response_element in response_elements:
            self.responses.append(response_element.getAttribute("name"))
        field_elements = element.getElementsByTagName("field")
        for field_element in field_elements:
            self.fields.append(Field(field_element, domains))

        bit_index = 0
        for field in self.fields:
            if field.getType() == "bit":
                field.bit_index = bit_index
                if bit_index == 0:
                    field.first_bit = True
                else:
                    field.first_bit = False
                bit_index += 1
                if bit_index == 8:
                    bit_index = 0
            else:
                bit_index = 0

        successor_is_bit = False
        for field in reversed(self.fields):
            if field.getType() == "bit":
                if successor_is_bit:
                    field.last_bit = False
                else:
                    field.last_bit = True
                successor_is_bit = True
            else:
                successor_is_bit = False


    def pythonName(self):
        toks = self.name.split("-")
        name = ""
        for tok in toks:
            name += tok.capitalize()
        return name
    python_name = property(pythonName)

    def __str__(self, indent=""):
        mstr = indent+"Method "+self.name+"\n"
        mstr += indent+"  synchronous: "+str(self.synchronous)+"\n"
        mstr += indent+"  index: "+str(self.index)+"\n"
        mstr += indent+"  label: "+self.label+"\n"
        mstr += indent+"  doc: "+self.doc+"\n"
        for rule in self.rules:
            mstr += rule.__str__(indent+"  ")
        for chassis in self.chassis:
            mstr += chassis.__str__(indent+"  ")
        for response in self.responses:
            mstr += indent+response
        for field in self.fields:
            mstr += field.__str__(indent+"  ")
        return mstr

class Class:
    def __init__(self, element, domains):
        self.name = None
        self.handler = None
        self.index = None
        self.label = None
        self.doc = None
        self.chassis = []
        self.rules = []
        self.methods = []

        if element == None:
            return

        self.name = element.getAttribute("name")
        self.handler = element.getAttribute("handler")
        self.index = int(element.getAttribute("index"))
        self.label = element.getAttribute("label")
        self.doc = getDoc(element)
        self.chassis = getChassis(element)
        self.rules = getRules(element)
        method_elements = element.getElementsByTagName("method")
        for element in method_elements:
            self.methods.append(Method(element, domains))

        method_dict = {}
        for method in self.methods:
            method_dict[method.name] = method
        for method in self.methods:
            for response in method.responses:
                method.response_methods.append(method_dict[response])

    def pythonName(self):
        toks = self.name.split("-")
        name = ""
        for tok in toks:
            name += tok.capitalize()
        return name
    python_name = property(pythonName)

    def __str__(self, indent=""):
        dstr = indent+"Domain "+self.name+"\n"
        dstr += indent+"  handler: "+str(self.handler)+"\n"
        dstr += indent+"  index: "+str(self.index)+"\n"
        dstr += indent+"  label: "+self.label+"\n"
        if self.doc:
            dstr += indent+"  doc: "+self.doc+"\n"
        for chassis in self.chassis:
            dstr += chassis.__str__(indent+"  ")
        for rule in self.rules:
            dstr += rule.__str__(indent+"  ")
        for method in self.methods:
            dstr += method.__str__(indent+"  ")
        return dstr

#######################################################################################################################

class Parser(object):

    def __init__(self, file_name):
        dom = parse(sys.argv[1])

        amqp = dom.getElementsByTagName("amqp")[0]
        self.major = int(amqp.getAttribute("major"))
        self.minor = int(amqp.getAttribute("minor"))
        self.revision = int(amqp.getAttribute("revision"))
        self.port = int(amqp.getAttribute("port"))
        self.comment = amqp.getAttribute("comment")

        self.constants = []
        constant_elements = dom.getElementsByTagName("constant")
        for element in constant_elements:
            self.constants.append(Constant(element))
        #for constant in self.constants:
        #    print(constant)

        self.domains = []
        domain_elements = dom.getElementsByTagName("domain")
        for element in domain_elements:
            self.domains.append(Domain(element))

        self.classes = []
        class_elements = dom.getElementsByTagName("class")
        for element in class_elements:
            self.classes.append(Class(element,self.getDomainMap()))
        for cls in self.classes:
            print("  "+cls.name)
        #    print(cls)

    def getDomainMap(self):
        dmap = {}
        for domain in self.domains:
            dmap[domain.name] = domain.typ
        return dmap

#######################################################################################################################
